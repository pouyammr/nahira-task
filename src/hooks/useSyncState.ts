/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { useState } from "react";

const useSyncState = (initialvalue?: any) => {
  const [value, updateValue] = useState(initialvalue);

  let latestvalue = value ?? null;
  
  const get = () => latestvalue;

  const set = (newValue: any) => {
    if (Array.isArray(newValue)) {
      latestvalue = [...newValue];
    }
    else if (typeof newValue === 'object' && newValue !== null) {
      latestvalue = {...newValue};
    }
    else
      latestvalue = newValue;
    updateValue(newValue);
    return latestvalue;
  }

  return {
    get,
    set,
  }
}

export default useSyncState;