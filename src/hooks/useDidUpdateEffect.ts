import { useEffect, useRef } from "react";

const useDidUpdateEffect = (fn: any, inputs: Array<any>) => {
  const didMountRef = useRef(false);
  useEffect(() => {
    if (didMountRef.current === true)
      fn();
    else
      didMountRef.current = true
  }, inputs)
}

export default useDidUpdateEffect;