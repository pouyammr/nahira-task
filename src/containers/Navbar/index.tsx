import React from 'react'
import styles from './Navbar.module.scss';
import Home from 'src/assets/home.svg';
import { Route, Link } from 'react-router-dom';
import { useHistory } from 'react-router';

interface NavBarProps {
  className?: string
}

const Navbar: React.FC<NavBarProps> = (props: NavBarProps) => {
  const { className } = props;
  const linkTitles = ["خانه", "بیمه شخص ثالث", "بیمه بدنه", "بیمه اشخاص", "شرکتهای بیمه"];
  const history = useHistory();
  
  return (
    <div className={className}>
    <Route path="/" render={() => (
    <nav className={styles['container']}>
      {linkTitles.map(el => (
        <Link to="/" key={el}>
          <div className={styles['nav-item']}>
            <img src={Home} className={styles['home']} alt="Home Icon" />
            <div className={styles['nav-title']}>{el}</div>
          </div>
        </Link>
      ))}
      <button className={styles['login-button']} onClick={() => history.push('/')}>
        <img src={Home} className={styles['home']} alt="Home Icon" />
        <div className={styles['nav-title']}>ورود / ثبت نام</div>
      </button>
    </nav>)} />
    </div>
  )
}

export default Navbar;