import React from 'react';
import styles from './Features.module.scss';

import CreditCard from 'src/assets/credit-card.svg';
import FreeDelivery from 'src/assets/free-delivery.svg';
import Analytics from 'src/assets/analytics.svg';
import Telephone from 'src/assets/telephone.svg';

import IconContainer from 'src/components/IconContainer';

const Features: React.FC = () => {
  const featureIcons = [
    {
      title: "مشاوره تخصصی بیمه",
      className: "telephone",
      src: Telephone
    },
    {
      title: "مقایسه قیمت بیمه",
      className: "analytics",
      src: Analytics
    },
    {
      title: "ارسال فوری",
      className: "delivery",
      src: FreeDelivery
    },
    {
      title: "خرید سریع و آسان",
      className: "credit-card",
      src: CreditCard
    }
  ]

  return (
    <div className={styles['container']}>
            <div className={styles['features']}>
        {featureIcons.map((el, index) => (
          <div className={styles['feature-icon']} key={index}>
            <IconContainer 
            checked={false} 
            className={styles['icon-container']}
            color={<img src={el.src} alt={`${el.className}`} className={styles[el.className]} />} />
          <div className={styles['feature-text']}>{el.title}</div>
          </div>
        ))}
      </div>
    </div>
  )
}

export default Features;