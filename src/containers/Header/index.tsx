import React from 'react';
import styles from './Header.module.scss';
import { useHistory, useLocation } from 'react-router';
import Navbar from 'src/containers/Navbar';
import LoginLogo from 'src/assets/login-logo.png';
import IconContainer from 'src/components/IconContainer';
import Personal from 'src/assets/personal.svg';
import PersonalWhite from 'src/assets/personal-white.svg';
import Travel from 'src/assets/travel-insurance.svg';
import TravelWhite from 'src/assets/travel-insurance-white.svg';
import Accident from 'src/assets/fender-bender.svg';
import AccidentWhite from 'src/assets/fender-bender-white.svg';
import Car from 'src/assets/insurance-color.svg';
import CarWhite from 'src/assets/insurance.svg';
import Bike from 'src/assets/motorbike.svg';
import BikeWhite from 'src/assets/motorbike-white.svg';
import Fire from 'src/assets/fire.svg';
import FireWhite from 'src/assets/fire-white.svg';
import Earthquake from 'src/assets/earthquake.svg';
import EarthquakeWhite from 'src/assets/earthquake-white.svg';

const Header: React.FC = () => {
  const history = useHistory();
  const location = useLocation<{checkedArray: boolean[]}>();
  console.log(location);
  const checkedArray = location?.state?.checkedArray ?? Array(7).fill(false);
  return (
    <div className={styles['container']}>
      <Navbar className={styles['navbar']} />
      <img src={LoginLogo} alt="Login Logo is Apbime" className={styles['login-logo']} />
      <IconContainer
      checked={checkedArray[0]} 
      color={<img src={Personal} className={styles['personal-icon']} />} 
      white={<img src={PersonalWhite} className={styles['personal-icon']} />}
      className={styles['personal-container']} 
      onClick={() => history.push("/", {
        checkedArray: [true, false , false , false, false, false, false]
      })} />
      <IconContainer
      checked={checkedArray[1]} 
      color={<img src={Travel} className={styles['travel-icon']} />} 
      white={<img src={TravelWhite} className={styles['travel-icon']} />}
      className={styles['travel-container']} 
      onClick={() => history.push("/", {
        checkedArray: [false, true , false , false, false, false, false]
      })} />
      <IconContainer
      checked={checkedArray[2]} 
      color={<img src={Accident} className={styles['accident-icon']} />} 
      white={<img src={AccidentWhite} className={styles['accident-icon']} />}
      className={styles['accident-container']} 
      onClick={() => history.push("/", {
        checkedArray: [false , false , true, false, false, false, false]
      })} />
      <IconContainer
      checked={checkedArray[3]} 
      color={<img src={Car} className={styles['car-icon']} />} 
      white={<img src={CarWhite} className={styles['car-icon']} />}
      className={styles['car-container']} 
      onClick={() => history.push("/", {
        checkedArray: [false , false , false, true, false, false, false]
      })} />
      <IconContainer
      checked={checkedArray[4]} 
      color={<img src={Bike} className={styles['bike-icon']} />} 
      white={<img src={BikeWhite} className={styles['bike-icon']} />}
      className={styles['bike-container']} 
      onClick={() => history.push("/",{
        checkedArray: [false , false , false, false, true, false, false]
      })} />
      <IconContainer
      checked={checkedArray[5]} 
      color={<img src={Fire} className={styles['fire-icon']} />} 
      white={<img src={FireWhite} className={styles['fire-icon']} />}
      className={styles['fire-container']} 
      onClick={() => history.push("/", {
        checkedArray: [false , false , false, false, false, true, false]
      })} />
      <IconContainer
      checked={checkedArray[6]} 
      color={<img src={Earthquake} className={styles['earthquake-icon']} />} 
      white={<img src={EarthquakeWhite} className={styles['earthquake-icon']} />}
      className={styles['earthquake-container']} 
      onClick={() => history.push("/", {
        checkedArray: [false , false , false, false, false, false, true]
      })} />    
    </div>
  )
}

export default Header;
