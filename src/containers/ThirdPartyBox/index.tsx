import React from 'react';
import styles from './ThirdPartyBox.module.scss';
import Select from 'src/components/Select';
import Toggle from 'src/components/Toggle';
import { useHistory } from 'react-router';

type Option = {
  title: string | number,
  value: string | number,
}

interface ThirdPartyBoxProps {
  vehicleTypeOptions: Option[],
  vehicleModelOptions: Option[],
  dueDateOptions: Option[],
  accidentDiscountOptions: Option[],
  insuranceDiscountOptions: Option[],
  manufacturingYearOptions: Option[],
  toggle: boolean,
  setToggle: () => void,
}

const ThirdPartyBox: React.FC<ThirdPartyBoxProps> = (props: ThirdPartyBoxProps) => {
  const { 
    vehicleTypeOptions,
    vehicleModelOptions,
    dueDateOptions, 
    accidentDiscountOptions,
    insuranceDiscountOptions,
    manufacturingYearOptions,
    toggle,
    setToggle
  } = props;
  const history = useHistory();

  return (
    <div className={styles['container']}>
      <div className={styles['third-party']}>
        <div className={styles['third-party__header']}>بیمه شخص ثالث</div>
        <div className={styles['third-party__content']}>
          <div className={styles['leasing']}>پرداخت اقساطی</div>
          <div className={styles['accident-discount']}>
            <div className={styles['input-box']}>
              <div className={styles['input-header']}>درصد تخفیف حوادث راننده</div>
                <Select className={styles['select']} placeholder="تخفیف حوادث راننده" options={accidentDiscountOptions} />
            </div>
          </div>
          <div className={styles['date-and-toggle']}>
            <div className={styles['input-box']}>
              <div className={styles['input-header']}>تاریخ سر رسید</div>
                <Select className={styles['select-tag']} placeholder="انتخاب تاریخ" options={dueDateOptions} />
              </div>
            <div className={styles['input-box']}>
            <div className={styles['input-header']}>بیمه نامه خسارت</div>
              <Toggle toggle={toggle} setToggle={setToggle}/>
            </div>
          </div>
          <div className={styles['vehicle-type']}>
            <div className={styles['input-box']}>
              <div className={styles['input-header']}>نوع وسیله نقلیه</div>
                <Select className={styles['select']} placeholder="انتخاب برند خودرو" options={vehicleTypeOptions} />
            </div>
          </div>
          <div className={styles['insurance-discount']}>
            <div className={styles['input-box']}>
              <div className={styles['input-header']}>درصد تخفیف بیمه نامه قبلی</div>
                <Select className={styles['select']} placeholder="تخفیف عدم خسارت" options={insuranceDiscountOptions} />
            </div>
          </div>
          <div className={styles['vehicle-model']}>
            <div className={styles['input-box']}>
              <div className={styles['input-header']}>مدل خودرو</div>
                <Select className={styles['select']} placeholder="انتخاب مدل خودرو" options={vehicleModelOptions} />
            </div>
          </div>
          <div className={styles['manufacturing-year']}>
            <div className={styles['input-box']}>
              <div className={styles['input-header']}>سال ساخت</div>
                <Select className={styles['select']} placeholder="انتخاب سال ساخت" options={manufacturingYearOptions} />
            </div>
          </div>
        </div>
      </div>
      <button className={styles['button']} onClick={()=> history.push("/price-inquiry")}>استعلام قیمت</button>
    </div>
  )
}

export default ThirdPartyBox;