import React from 'react';
import styles from './CardContainer.module.scss';

import ThirdParty from 'src/assets/third-party.png';
import Body from 'src/assets/body.png';
import Fire from 'src/assets/fire.png';
import Earthquake from 'src/assets/earthquake.png';

import Card from 'src/components/Card';

const CardContainer: React.FC = () => (
  <div className={styles['container']}>
    <Card 
    image={<img src={ThirdParty} />} 
    author="" 
    header="بیمه شخص ثالث"
    content="بیمه ‌آتش‌سوزی یکی از قدیمی‌ترین رشته‌های بیمه در دنیا است. اهمیت این بیمه تا آنجا است که در کشورهای توسعه‌یافته، تقریباً هیچ دارایی یا مالی را نمی‌توان یافت که بیمه آتش‌سوزی نداشته باشد."
    date={1601497800000} />
    <Card 
    image={<img src={Body} />} 
    author="" 
    header="بیمه بدنه"
    content="بیمه ‌آتش‌سوزی یکی از قدیمی‌ترین رشته‌های بیمه در دنیا است. اهمیت این بیمه تا آنجا است که در کشورهای توسعه‌یافته، تقریباً هیچ دارایی یا مالی را نمی‌توان یافت که بیمه آتش‌سوزی نداشته باشد."
    date={1601497800000} />
    <Card 
    image={<img src={Earthquake} />} 
    author="" 
    header="بیمه زلزله"
    content="بیمه ‌آتش‌سوزی یکی از قدیمی‌ترین رشته‌های بیمه در دنیا است. اهمیت این بیمه تا آنجا است که در کشورهای توسعه‌یافته، تقریباً هیچ دارایی یا مالی را نمی‌توان یافت که بیمه آتش‌سوزی نداشته باشد."
    date={1601497800000} />
    <Card 
    image={<img src={Fire} />} 
    author="" 
    header="بیمه آتش سوزی"
    content="بیمه ‌آتش‌سوزی یکی از قدیمی‌ترین رشته‌های بیمه در دنیا است. اهمیت این بیمه تا آنجا است که در کشورهای توسعه‌یافته، تقریباً هیچ دارایی یا مالی را نمی‌توان یافت که بیمه آتش‌سوزی نداشته باشد."
    date={1601497800000} />
  </div>
)

export default CardContainer;