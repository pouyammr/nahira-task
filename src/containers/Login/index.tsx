import React, { useState } from 'react';
import styles from './Login.module.scss';
import allActions from 'src/store/actions/allActions';
import { useSelector, useDispatch } from 'react-redux';
import useDidUpdateEffect from 'src/hooks/useDidUpdateEffect';
import useSyncState from 'src/hooks/useSyncState';
import { RootState } from 'src/store/store';
import { useHistory, useRouteMatch } from 'react-router';

const Login: React.FC = () => {
  const inputError = useSelector((state: RootState) => state.users.inputError);
  const login = useSelector((state: RootState) => state.users.login);
  const currentUser = useSelector((state: RootState) => state.users.currentUser);
  const [usernameInput, setUsernameInput] = useState<string>("");
  const [passwordInput, setPasswordInput] = useState<string>("");
  const errorMessage = useSyncState(null);
  const dispatch = useDispatch();
  const history = useHistory();
  const match = useRouteMatch();

  const handleUsername = (event: any) => {
    setUsernameInput(event.target.value)
  }

  const handlePassword = (event: any) => {
    setPasswordInput(event.target.value)
  }

  const handleSubmit = () => {
    dispatch(allActions.userActions.checkUser(usernameInput, passwordInput));
  }

  useDidUpdateEffect(() => {
    inputError
    && errorMessage.set(<div className={styles['error']}>.نام کاربری باید آدرس ایمیل و رمز عبور باید شامل حروف کوچک و بزرک انگلیسی و اعداد باشد</div>)
  },[inputError])

  useDidUpdateEffect(() => {
    if (!login) {
      errorMessage.set(<div className={styles['error']}>.نام کاربری یا رمز عبور اشتباه است. دوباره امتحان کنید</div>)
    }
    else {
      errorMessage.set(<div className={styles['login']}>.شما با موفقیت وارد شدید</div>)
      
      setTimeout(() => {
        if (match.url === "/login")
          history.goBack()
        else
          history.push("/home")
      },1500)
    }
  }, [login])

  return (
    <div className={styles['backdrop']}>
      <div className={styles['login-container']}>
        <div className={styles['input-box']}>
          <div className={styles['box-title']}>:نام کاربری خود را وارد کنید</div>
          <input type="text" placeholder="نام کاربری" className={styles['box-input']} onChange={event => handleUsername(event)} />
        </div>
        <div className={styles['input-box']}>
          <div className={styles['box-title']}>:رمز عبور خود را وارد کنید</div>
          <input type="password" placeholder="رمز عبور" className={styles['box-input']} onChange={event => handlePassword(event)} />
        </div>
        {errorMessage.get()}
        <div className={styles['buttons']}>
          <button className={styles['submit']} onClick={handleSubmit}>ورود</button>
        </div>
      </div>
    </div>
  )
}

export default Login;