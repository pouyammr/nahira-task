import React, { useState } from 'react';
import styles from './ThirdParty.module.scss';

import Shape from 'src/assets/shape.png';
import Bimeh from 'src/assets/bimeh.png';

import ThirdPartyBox from 'src/containers/ThirdPartyBox';
import Features from 'src/containers/Features';
import Reminder from 'src/containers/Reminder';
import CardContainer from 'src/containers/CardContainer';

const ThirdParty: React.FC = () => {
const [toggle, setToggle] = useState<boolean>(false);
const [name, setName] = useState<string>("");
const [phoneNumber, setPhoneNumber] = useState<string>(""); 

  return (
    <div className={styles['container']}>
      <ThirdPartyBox
      vehicleTypeOptions={[]}
      vehicleModelOptions={[]}
      dueDateOptions={[]}
      accidentDiscountOptions={[]}
      insuranceDiscountOptions={[]}
      manufacturingYearOptions={[]}
      toggle={toggle}
      setToggle={() => setToggle(!toggle)} />
      <img src={Shape} className={styles['shape-right']} />
      <Features />
      <img src={Shape} className={styles['shape-left']} />
      <Reminder 
      onChangeName={event => setName(event.target.value)}
      onChangePhoneNumber={event => setPhoneNumber(event.target.value)} />
      <CardContainer />
      <img src={Bimeh} className={styles['bimeh']} />
    </div>
  )
}

export default ThirdParty;