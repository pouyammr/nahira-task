import React from 'react';
import styles from './Reminder.module.scss';

import AnnounceLeft from 'src/assets/announce-left.svg';
import AnnounceRight from 'src/assets/announce-right.svg';

import Select from 'src/components/Select';
import Input from 'src/components/Input';

interface ReminderProps {
  onChangeName: (event: any) => void,
  onChangePhoneNumber: (event: any) => void
}

const Reminder: React.FC<ReminderProps> = (props: ReminderProps) => {
  const { onChangeName, onChangePhoneNumber} = props;

  return (
    <>
      <div className={styles['reminder']}>
        <div className={styles['reminder__first-row']}>
          <img src={AnnounceLeft} className={styles['announce']} />
          <div className={styles['reminder__header']}>یادآور تمدید یا اقساط انواع بیمه نامه ها</div>
          <img src={AnnounceRight} className={styles['announce']} />
        </div>
        <div className={styles['reminder__second-row']}>
          <Select options={[]} placeholder="نوع بیمه" className={styles['reminder-select']} />
          <Select options={[]} placeholder="تاریخ سررسید" className={styles['reminder-select']} />
          <Input placeholder="نام و نام خانوادگی" className={styles['reminder-input']} onChange={event => onChangeName(event)} />
          <Input placeholder="شماره موبایل" className={styles['reminder-input']} onChange={event => onChangePhoneNumber(event)} />
        </div>
        <div className={styles['reminder__third-row']}>
          <button className={styles['button']}>ثبت اطلاعات</button>
        </div>
      </div>
    </>
  )
}

export default Reminder;