import React from 'react';
import styles from './Footer.module.scss';
import LoginLogo from 'src/assets/login-logo.png';
import Instagram from 'src/assets/instagram.svg';
import Telegram from 'src/assets/telegram.svg';
import YouTube from 'src/assets/youtube.svg';

const Footer: React.FC = () => {
  const logos = [{
    title: "instagram",
    src: Instagram
  },
  {
    title: "telegram",
    src: Telegram
  },
  {
    title: "youtube",
    src: YouTube
  }];
  const contactInfo = [{
    title: "تلفن پشتیبانی:",
    phoneNumber: "۰۹۱۲−۰۳۸۴۳۵۶"
  },
  {
    title: "تلفن دفتر:",
    phoneNumber: "۰۱۳−۳۲۱۳۰۵۲۱"
  },
  {
    title: "امور مالی:",
    phoneNumber: "۰۹۱۲−۰۳۸۴۳۵۶"
  },
  {
    title: "شماره فکس:",
    phoneNumber: "۰۱۳−۳۲۱۳۰۵۲۱"
  }]

  return (  
    <div className={styles['container']}>
      <div className={styles['footer-upper']}>
        <div className={styles['info']}>
          <div className={styles['info__ad']}>به خانواده آسان پرداخت بیمه بپیوندید</div>
          <p className={styles['info__text']}>وب سایت آسان پرداخت بیمه همیشه و در هر جایی در دسترس است. اطلاعات درخواستی رو تکمیل کن و بلا فاصله قیمت و شرایط شرکتهای بیمه رو ببین، مـقایسه کن، مشاوره بگیر و شرکت بیمه ای رو که دوسـت داری انتخاب کن. تمام شد به همین سادگی. بــــیمه نامه هر جایی که دوست دارید تقدیم شما میشه</p>
        </div>
        <div className={styles['contact']}>
          <img src={LoginLogo} className={styles['login-logo']} alt="Login logo is Apbime" />
          <div className={styles['contact__info']}>
            {contactInfo.map((el ,index) => (
              <div className={styles['contact__info__row']} key={index}>
                <div className={styles['row-title']}>{el.title}</div>
                <div className={styles['row-number']}>{el.phoneNumber}</div>
              </div>
            ))}
          </div>
        </div>
      </div>
      <div className={styles['footer-lower']}>
        <div className={styles['logos']}>
          {logos.map((el, index) => (
            <a href="#" key={index} className={styles['logo']}>
              <img src={el.src} alt={`${el.title} link for Apbime`} className={styles[`${el.title}`]} />
            </a>
          ))}
        </div>
      </div>
    </div>
  )
}

export default Footer;