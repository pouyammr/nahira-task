/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { AnyAction } from 'redux';
import actionTypes from 'src/store/actions/actionTypes';

const initialState = {
  users: [{username: "test@example.com", password: "Abbas1234", login: false}],
  inputError: null,
  login: null,
  currentUser: null,
};

const users = (state: any = initialState, action: AnyAction) => {
  const usernameRegEX = /(\S+)@(\S+).(\S+)/;
  const passwordRegEX = /^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z])[A-Za-z0-9]*$/gm

  switch (action.type) {
    
    case actionTypes.checkUser:
      if (!usernameRegEX.test(action.payload.username) || !passwordRegEX.test(action.payload.password))
        return {
          ...state,
          inputError: true
        }
      else {
        const user = state.users.find((el: any) => el.username === action.payload.username);
        if (user === undefined) {
          return {
            ...state,
            login: false,
            inputError: null
          }
        }
        else {
          if (action.payload.password === user.password) {
            user.login = true
            return {
              ...state,
              users: [...state.users, user],
              inputError: null,
              login: user.login,
              currentUser: user.username,
            }
          }
          else {
            return {
              ...state,
              login: false
            }
          }
        }
      }
    default:
      return state;
  }
}

export default users;