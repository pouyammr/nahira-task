import users from "./reducers/users";
import { createStore, combineReducers } from "redux";

const reducers = {users};
const rootReducer = combineReducers(reducers);

const store = createStore(rootReducer);
export type RootState = ReturnType<typeof rootReducer>
export default store;