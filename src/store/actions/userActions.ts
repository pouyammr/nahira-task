/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import actionTypes from "./actionTypes";

const checkUser = (username: string, password: string) => {
  return {
    type: actionTypes.checkUser,
    payload: {
      username: username,
      password: password,
    }
  }
}

export default {
  checkUser
}