import React from 'react';
import styles from './App.module.scss';
import { BrowserRouter } from 'react-router-dom';
import { Route } from 'react-router';
import Header from 'src/containers/Header';
import Footer from 'src/containers/Footer';
import ThirdParty from 'src/containers/ThirdParty';
import Login from 'src/containers/Login';
function App() {
  return (
    <BrowserRouter>
      <div className={styles['App']}>
        <header className={styles['header']}>
          <Header />
        </header>
        <main>
          <Route path="/" exact component={Login} />
          <Route path="/home" exact component={ThirdParty} />
        </main>
        <footer>
          <Footer />
        </footer>
      </div>
    </BrowserRouter>
  );
}

export default App;
