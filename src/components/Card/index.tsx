import React from 'react';
import styles from './Card.module.scss';
import { NavLink } from 'react-router-dom';
import moment from 'moment-jalaali';

import User from 'src/assets/user.svg';
import Calendar from 'src/assets/calendar.svg';

moment.loadPersian({usePersianDigits: true, dialect: 'persian-modern'});

interface CardProps {
  image: React.ReactNode,
  date: number,
  header: string,
  content: string,
  author: string,
}

const Card: React.FC<CardProps> = (props: CardProps) => {
  const { image, date, header, content, author } = props;
  const cardDate = moment(date).format("jMMM / jYYYY / jDD");
  
  return (
    <div className={styles['card']}>
      <div className={styles['card-image']}>
        {image}
        <div className={styles['gradient']} />
      </div>
      <div className={styles['card-content']}>
        <div className={styles['card-info']}>
          <div className={styles['card-author']}>
            <img src={User} className={styles['user']} />
            <div className={styles['author']}>{author}</div>
          </div>
          <div className={styles['card-date']}>
            <div className={styles['date']}>{cardDate}</div>
            <img src={Calendar} className={styles['calendar']} />
          </div>
        </div>
        <div className={styles['header']}>{header}</div>
        <p className={styles['content']}>{content}</p>
        <NavLink to="/" className={styles['continue']}>
          <div className={styles['continue__text']}>ادامه مطلب</div>
        </NavLink>
      </div>
    </div>
  )
}

export default Card;