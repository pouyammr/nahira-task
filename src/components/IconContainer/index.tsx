import React from 'react';
import styles from './IconContainer.module.scss';
import cn from 'classnames';

interface IconContainerProps {
  checked: boolean,
  className?: string,
  color: React.ReactNode,
  white?: React.ReactNode,
  onClick?: () => void
}
const IconContainer: React.FC<IconContainerProps> = (props: IconContainerProps) => {
  const { className, color, white, checked, onClick } = props; 
  const handleClick = () => {
    onClick?.();
  }
  
  return (
    <div 
    className={checked
    ? cn(styles['container'], className, styles['checked']) 
    : cn(styles['container'], className)}
    onClick={handleClick}>
     {checked? white : color}
    </div>
  )
}

export default IconContainer;