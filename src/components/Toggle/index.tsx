import React from 'react';
import styles from './Toggle.module.scss';
import cn from 'classnames';

interface ToggleProps {
  toggle: boolean,
  setToggle: () => void,
  className?: string
}

const Toggle: React.FC<ToggleProps> = (props: ToggleProps) => {
  const { toggle, setToggle, className } = props;

  return (
    <div className={!toggle
      ? cn(styles['container'], className)
      : cn(styles['container'], styles['container__on'], className)} 
      onClick={setToggle}>
        <div className={styles['toggle-button']} />
      </div>
  )
}

export default Toggle;