import React from 'react';
import styles from './Select.module.scss';
import cn from 'classnames';

type Option = {
  title: string | number,
  value: string | number
}

interface SelectProps {
  options: Option[],
  placeholder: string,
  className?: string
}

const Select: React.FC<SelectProps> = (props: SelectProps) => {
  const { options, placeholder, className } = props;

  return (
    <div className={styles['container']}>
      <select className={cn(styles['select'], className)} required name="options">
        <option value="" disabled selected hidden className={styles['placeholder']}>{placeholder}</option>
        {options.map(item => {
          <option value={item.value}>{item.title}</option>
        })}
      </select>
    </div>
  )
}

export default Select;
