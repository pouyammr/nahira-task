import React from 'react';
import styles from './Input.module.scss';
import cn from 'classnames';

interface InputProps {
  placeholder: string,
  onChange: (event: any) => void,
  className?: string
}

const Input: React.FC<InputProps> = (props: InputProps) => {
  const { placeholder, onChange, className } = props;

  return (
    <div className={styles['container']}>
      <input 
      className={cn(styles['input'], className)} 
      placeholder={placeholder}
      onChange={event => onChange(event)} />
    </div>
  )
}

export default Input;